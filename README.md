# Okapi Linguistic Tools #

### What is this repository for? ###

Okapi sub-project dedicated to deep, language specific analysis and its applications. Spelling and grammar checks, term and blacklisted term checks, term extraction etc..

### How do I get set up? ###

Requires Java 11 and maven >= 3.3.3


