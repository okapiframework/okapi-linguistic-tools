package net.sf.okapi.lib.languagetool.rules;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import net.sf.okapi.common.LocaleId;
import net.sf.okapi.lib.languagetool.LanguageToolUtil;

@RunWith(JUnit4.class)
public class LanguageToolUtilTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void miscLanguages() {
		assertEquals("en-US", LanguageToolUtil.getLanguage(new LocaleId("en", "US")).getShortCodeWithCountryAndVariant());
		assertEquals("en-GB", LanguageToolUtil.getLanguage(new LocaleId("en", "GB")).getShortCodeWithCountryAndVariant());

		assertEquals("fr", LanguageToolUtil.getLanguage(LocaleId.FRENCH).getShortCodeWithCountryAndVariant());
		assertEquals("fr", LanguageToolUtil.getLanguage(new LocaleId("fr", "FR")).getShortCodeWithCountryAndVariant());
		
		assertEquals("ja-JP", LanguageToolUtil.getLanguage(LocaleId.JAPANESE).getShortCodeWithCountryAndVariant());		
		
		assertEquals("es", LanguageToolUtil.getLanguage(LocaleId.SPANISH).getShortCodeWithCountryAndVariant());
		assertEquals("es", LanguageToolUtil.getLanguage(new LocaleId("es", "MX")).getShortCodeWithCountryAndVariant());
		
		assertEquals("zh-CN", LanguageToolUtil.getLanguage(new LocaleId("zh", "CN")).getShortCodeWithCountryAndVariant());		
		// hum, this really should be zh-TW but LT only returns zh-CN
		assertEquals("zh-CN", LanguageToolUtil.getLanguage(new LocaleId("zh", "TW")).getShortCodeWithCountryAndVariant());		
		assertEquals("ar", LanguageToolUtil.getLanguage(LocaleId.ARABIC).getShortCodeWithCountryAndVariant());
	}
	
	@Test
	public void portuguese() {
		assertEquals("pt-PT", LanguageToolUtil.getLanguage(new LocaleId("pt", "PT")).getShortCodeWithCountryAndVariant());
		assertEquals("pt-BR", LanguageToolUtil.getLanguage(new LocaleId("pt", "BR")).getShortCodeWithCountryAndVariant());
	}
	
	@Test
	public void german() {
		assertEquals("de-DE", LanguageToolUtil.getLanguage(LocaleId.GERMAN).getShortCodeWithCountryAndVariant());
		assertEquals("de-DE", LanguageToolUtil.getLanguage(new LocaleId("de", "DE")).getShortCodeWithCountryAndVariant());
		assertEquals("de-DE", LanguageToolUtil.getLanguage(new LocaleId("de", "BE")).getShortCodeWithCountryAndVariant());
		assertEquals("de-DE", LanguageToolUtil.getLanguage(new LocaleId("de", "LI")).getShortCodeWithCountryAndVariant());
		assertEquals("de-DE", LanguageToolUtil.getLanguage(new LocaleId("de", "LU")).getShortCodeWithCountryAndVariant());
	}
}
