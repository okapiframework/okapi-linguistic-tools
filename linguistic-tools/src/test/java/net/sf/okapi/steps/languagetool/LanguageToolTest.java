/*===========================================================================
  Copyright (C) 2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.steps.languagetool;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.IOException;
import java.util.LinkedList;
import java.util.Locale;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.languagetool.Languages;

import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.annotation.GenericAnnotation;
import net.sf.okapi.common.annotation.GenericAnnotationType;
import net.sf.okapi.common.annotation.ITSLQIAnnotations;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.TextFragment;
import net.sf.okapi.common.resource.TextFragment.TagType;
import net.sf.okapi.common.resource.TextUnit;
import net.sf.okapi.lib.terminology.ConceptEntry;
import net.sf.okapi.lib.verification.BlackTerm;

@RunWith(JUnit4.class)
public class LanguageToolTest {

	private LanguageTool lt;
	private final LocaleId srcLoc = new LocaleId("en", "us");
	private final LocaleId trgLoc = new LocaleId("fr", "fr");

	@Before
	public void setUp () throws IOException {
		lt = new LanguageTool(null, srcLoc, trgLoc);
	}
	
	@After
	public void tearDown () {
		lt.shutDown();
	}

	@Test
	public void simpleTest1 () {
		ITextUnit tu = new TextUnit("id", "original teext");
		tu.setTargetContent(trgLoc, new TextFragment("texte original"));
		lt.run(tu);
		ITSLQIAnnotations anns = tu.getSource().getAnnotation(ITSLQIAnnotations.class);
		assertNotNull(anns);
		assertEquals(1, anns.getAllAnnotations().size());
		assertEquals("misspelling", anns.getFirstAnnotation(GenericAnnotationType.LQI).getString(GenericAnnotationType.LQI_TYPE));
		anns = tu.getTarget(trgLoc).getAnnotation(ITSLQIAnnotations.class);
		assertNull(anns);
	}

	@Test
	public void testLTLanguages () {
		assertEquals("en-US", Languages.getLanguageForLocale(new Locale("en", "US")).getShortCodeWithCountryAndVariant());
		assertEquals("en-US", Languages.getLanguageForShortCode("en-US").getShortCodeWithCountryAndVariant());
		assertEquals("en-US", Languages.getLanguageForLocale(new Locale("en", "us")).getShortCodeWithCountryAndVariant());
		assertEquals("en-US", Languages.getLanguageForShortCode("en-us").getShortCodeWithCountryAndVariant());
	}
	
	@Test
	public void simpleTest2 () {
		ITextUnit tu = new TextUnit("id", "File not found: %1");
		tu.setTargetContent(trgLoc, new TextFragment("Fichier non trouv\u00e9: %1"));
		lt.run(tu);
		ITSLQIAnnotations anns = tu.getSource().getAnnotation(ITSLQIAnnotations.class);
		assertNull(anns);
		anns = tu.getTarget(trgLoc).getAnnotation(ITSLQIAnnotations.class);
		assertNotNull(anns);
		assertEquals(1, anns.getAllAnnotations().size());
	}

	@Test
	public void testWithCodes () {
		TextFragment tf = new TextFragment();
		tf.append(TagType.OPENING, "b", "<b>");
		tf.append("This is a short setence.");
		tf.append(TagType.CLOSING, "b", "</b>");
		ITextUnit tu = new TextUnit("id");
		tu.setSourceContent(tf);
		tu.setTargetContent(trgLoc, tf.clone());
		lt.run(tu);
		ITSLQIAnnotations anns = tu.getTarget(trgLoc).getAnnotation(ITSLQIAnnotations.class);
		assertNotNull(anns);
		assertEquals(1, anns.getAllAnnotations().size());
		assertEquals("misspelling", anns.getFirstAnnotation(GenericAnnotationType.LQI).getString(GenericAnnotationType.LQI_TYPE));
	}
	
	@Test
	public void termCheckTbxWithEmptyLists() throws IOException {
		Parameters p = new Parameters();
		p.setTermCheck(true);
		p.setBlacklistCheck(true);
		p.setCheckGrammar(false);
		p.setCheckSpelling(false);
		p.setTermCheckPath(null);
	    p.setBlacklistCheckPath(null);
		LanguageTool langTool = new LanguageTool(p, LocaleId.ENGLISH, LocaleId.fromString("hu"), new LinkedList<ConceptEntry>(), new LinkedList<BlackTerm>());
		
		// term check rule will fire as the target term is not exactly as it should be
		ITextUnit tu = new TextUnit("id", "Alpha smoothing factor");
		tu.setTargetContent(LocaleId.fromString("hu"), new TextFragment("tényező simítási Alfax"));
		langTool.run(tu);
		ITSLQIAnnotations anns = tu.getTarget(LocaleId.fromString("hu")).getAnnotation(ITSLQIAnnotations.class);
		assertNull(anns);
	}
	
	@Test
	public void termCheckTbx() throws IOException {
		Parameters p = new Parameters();
		p.setTermCheck(true);
		p.setCheckGrammar(false);
		p.setCheckSpelling(false);
		p.setTermCheckPath(FileLocation.fromClass(this.getClass()).in("/test01.tbx").asPath().toAbsolutePath().toString());
		LanguageTool langTool = new LanguageTool(p, LocaleId.ENGLISH, LocaleId.fromString("hu"));
		
		// term check rule will fire as the target term is not exactly as it should be
		ITextUnit tu = new TextUnit("id", "Alpha smoothing factor");
		tu.setTargetContent(LocaleId.fromString("hu"), new TextFragment("tényező simítási Alfax"));
		langTool.run(tu);
		ITSLQIAnnotations anns = tu.getTarget(LocaleId.fromString("hu")).getAnnotation(ITSLQIAnnotations.class);
		assertNotNull(anns);
		assertEquals(1, anns.getAllAnnotations().size());
		
		// no rule should fire, exact match
		ITextUnit tu2 = new TextUnit("id2", "Alpha smoothing factor");
		tu2.setTargetContent(LocaleId.fromString("hu"), new TextFragment("Alfa simítási tényező"));
		langTool.run(tu2);
		anns = tu2.getTarget(LocaleId.fromString("hu")).getAnnotation(ITSLQIAnnotations.class);
		assertNull(anns);
		
		// no rule should fire, stemming difference only
		ITextUnit tu3 = new TextUnit("id3", "Alpha smooth factors");
		tu3.setTargetContent(LocaleId.fromString("hu"), new TextFragment("Alfa simítási tényező"));
		langTool.run(tu3);
		anns = tu3.getTarget(LocaleId.fromString("hu")).getAnnotation(ITSLQIAnnotations.class);
		assertNull(anns);
	}
	
	@Test
	public void termCheckIATETbx() throws IOException {
		Parameters p = new Parameters();
		p.setTermCheck(true);
		p.setCheckGrammar(false);
		p.setCheckSpelling(false);
		p.setTermCheckPath(FileLocation.fromClass(this.getClass()).in("/IATE_export_11112016_language(cs_en_es).tbx").asPath().toAbsolutePath().toString());
		LanguageTool langTool = new LanguageTool(p, LocaleId.ENGLISH, LocaleId.fromString("es"));
		
		// term check rule will fire as the target term is not exactly as it should be
		ITextUnit tu = new TextUnit("id", "The byproducts are waste from mineral dressing");
		tu.setTargetContent(LocaleId.fromString("es"), new TextFragment("Los subproductos son residuos de la disposición de minerales"));
		langTool.run(tu);
		ITSLQIAnnotations anns = tu.getTarget(LocaleId.fromString("es")).getAnnotation(ITSLQIAnnotations.class);
		assertNotNull(anns);
		assertEquals(1, anns.getAllAnnotations().size());
		
		// no rule should fire, exact match
		ITextUnit tu2 = new TextUnit("id2", "The byproducts are waste from mineral dressing");
		tu2.setTargetContent(LocaleId.fromString("es"), new TextFragment("Los subproductos son residuos de la preparación de minerales"));
		langTool.run(tu2);
		anns = tu2.getTarget(LocaleId.fromString("es")).getAnnotation(ITSLQIAnnotations.class);
		assertNull(anns);
		
		// rules should fire error in target term
		tu2.setTargetContent(LocaleId.fromString("es"), new TextFragment("Los subproductos son resixduos de la preparación de minerales"));
		langTool.run(tu2);
		anns = tu2.getTarget(LocaleId.fromString("es")).getAnnotation(ITSLQIAnnotations.class);
		assertNotNull(anns);
		// two rules fire one for larger term and one for residuos
		assertEquals(2, anns.getAllAnnotations().size());
		
		// no rule should fire, source stemming difference only
		ITextUnit tu2a = new TextUnit("id2a", "The byproducts are metallic minerals");
		tu2a.setTargetContent(LocaleId.fromString("es"), new TextFragment("Los subproductos son minerales metálicos"));
		langTool.run(tu2a);
		anns = tu2a.getTarget(LocaleId.fromString("es")).getAnnotation(ITSLQIAnnotations.class);
		assertNull(anns);
		
		// rule should fire one missing term
		tu2a.setTargetContent(LocaleId.fromString("es"), new TextFragment("Los subproductos son metálicos"));
		langTool.run(tu2a);
		anns = tu2a.getTarget(LocaleId.fromString("es")).getAnnotation(ITSLQIAnnotations.class);
		assertNotNull(anns);
		// two rules fire one for larger term and one for minerales
		assertEquals(2, anns.getAllAnnotations().size());
				
		// no rule should fire, target stemming difference only
		ITextUnit tu3 = new TextUnit("id3", "The byproducts are waste from mineral dressing");
		tu3.setTargetContent(LocaleId.fromString("es"), new TextFragment("El subproducto es residuo de la preparación de minerales"));
		langTool.run(tu3);
		anns = tu3.getTarget(LocaleId.fromString("es")).getAnnotation(ITSLQIAnnotations.class);
		assertNull(anns);
		
		// rule should fire extra term
		tu3.setTargetContent(LocaleId.fromString("es"), new TextFragment("El subproducto es residuo grandes de la preparación de minerales"));
		langTool.run(tu3);
		anns = tu3.getTarget(LocaleId.fromString("es")).getAnnotation(ITSLQIAnnotations.class);
		assertNotNull(anns);
		assertEquals(1, anns.getAllAnnotations().size());
		
		// no rule should fire, target stemming difference only
		ITextUnit tu4 = new TextUnit("id4", "The competence of the Member States");
		tu4.setTargetContent(LocaleId.fromString("es"), new TextFragment("Las competencias de los Estados miembros"));
		langTool.run(tu4);
		anns = tu4.getTarget(LocaleId.fromString("es")).getAnnotation(ITSLQIAnnotations.class);
		assertNull(anns);
	}
	
	@Test
	public void termCheckSdlTbx() throws IOException {
		Parameters p = new Parameters();
		p.setTermCheck(true);
		p.setCheckGrammar(false);
		p.setCheckSpelling(false);
		p.setTermCheckPath(FileLocation.fromClass(this.getClass()).in("/sdl_tbx.tbx").asPath().toAbsolutePath().toString());
		LanguageTool langTool = new LanguageTool(p, LocaleId.ENGLISH, LocaleId.GERMAN);
		
		// morphological variants match for both english and german
		// "typeface family" and "Schriftfamilie"
		ITextUnit tu = new TextUnit("id", "The typeface families define all fonts.");
		tu.setTargetContent(LocaleId.GERMAN, new TextFragment("Die Schriftfamilien definieren alle Schriften."));
		langTool.run(tu);
		ITSLQIAnnotations anns = tu.getTarget(LocaleId.GERMAN).getAnnotation(ITSLQIAnnotations.class);
		assertNull(anns);
		
		// A literal translation "font group" = "Schriftgruppen" is caught as an error
		ITextUnit tu2 = new TextUnit("id", "The typeface families define all fonts.");
		tu2.setTargetContent(LocaleId.GERMAN, new TextFragment("Die Schriftgruppen definieren alle Schriften."));
		langTool.run(tu2);
		anns = tu2.getTarget(LocaleId.GERMAN).getAnnotation(ITSLQIAnnotations.class);
		assertNotNull(anns);
		assertEquals(1, anns.getAllAnnotations().size());
	}
	
	@Test
	public void termCheckCsv() throws IOException {
		Parameters p = new Parameters();
		p.setTermCheck(true);
		p.setCheckGrammar(false);
		p.setCheckSpelling(false);
		p.setTermCheckPath(FileLocation.fromClass(this.getClass()).in("/test01.csv").asPath().toAbsolutePath().toString());
		
		LanguageTool langTool = new LanguageTool(p, LocaleId.ENGLISH, LocaleId.FRENCH);
		
		ITextUnit tu = new TextUnit("id", "This is a sentence with source 1.");
		tu.setTargetContent(LocaleId.FRENCH, new TextFragment("This is a sentence without the target term."));
		langTool.run(tu);
		ITSLQIAnnotations anns = tu.getTarget(LocaleId.FRENCH).getAnnotation(ITSLQIAnnotations.class);
		assertNotNull(anns);
		assertEquals(1, anns.getAllAnnotations().size());
		
		// no rule should fire, exact match
		ITextUnit tu2 = new TextUnit("id2", "This is a sentence with source 1.");
		tu2.setTargetContent(LocaleId.FRENCH, new TextFragment("This is a sentence with target 1."));
		langTool.run(tu2);
		anns = tu2.getTarget(LocaleId.FRENCH).getAnnotation(ITSLQIAnnotations.class);
		assertNull(anns);
		
		// no rule should fire
		ITextUnit tu3 = new TextUnit("id3", "This is a sentence with source 3.");
		tu3.setTargetContent(LocaleId.FRENCH, new TextFragment("This is a sentence with target 3."));
		langTool.run(tu3);
		anns = tu3.getTarget(LocaleId.FRENCH).getAnnotation(ITSLQIAnnotations.class);
		assertNull(anns);
	}

	@Test
	public void termCheckPlural() throws IOException {
		Parameters p = new Parameters();
		p.setTermCheck(true);
		p.setCheckGrammar(false);
		p.setCheckSpelling(false);
		p.setTermCheckPath(FileLocation.fromClass(this.getClass()).in("/plural.csv").asPath().toAbsolutePath().toString());

		LanguageTool langTool = new LanguageTool(p, LocaleId.ENGLISH, LocaleId.FRENCH);

		ITSLQIAnnotations anns;

		ITextUnit tu2 = new TextUnit("id2", "This has windows plural.");
		tu2.setTargetContent(LocaleId.FRENCH, new TextFragment("This doesn't have translated windows plural."));
		langTool.run(tu2);
		anns = tu2.getTarget(LocaleId.FRENCH).getAnnotation(ITSLQIAnnotations.class);
		assertNotNull(anns);
		assertEquals(1, anns.getAllAnnotations().size());

		ITextUnit tu3 = new TextUnit("id3", "This has oxen plural.");
		tu3.setTargetContent(LocaleId.FRENCH, new TextFragment("This doesn't have translated oxen plural."));
		langTool.run(tu3);
		anns = tu3.getTarget(LocaleId.FRENCH).getAnnotation(ITSLQIAnnotations.class);
		assertNotNull(anns);
		assertEquals(1, anns.getAllAnnotations().size());
	}
	
	@Test
	public void termCheckBlacklist() throws IOException {
		Parameters p = new Parameters();
		p.setTermCheck(false);
		p.setBlacklistCheck(true);
		p.setCheckGrammar(false);
		p.setCheckSpelling(false);
		p.setBlacklistCheckPath(FileLocation.fromClass(this.getClass()).in("/black_tsv_simple.txt").asPath().toAbsolutePath().toString());
		LanguageTool langTool = new LanguageTool(p, LocaleId.ENGLISH, LocaleId.FRENCH);
		
		ITextUnit tu = new TextUnit("id", "This is a sentence with the blackterm.");
		tu.setTargetContent(LocaleId.FRENCH, new TextFragment("This is a sentence with the blackterm1."));
		langTool.run(tu);
		ITSLQIAnnotations anns = tu.getTarget(LocaleId.FRENCH).getAnnotation(ITSLQIAnnotations.class);
		assertNotNull(anns);
		assertEquals(1, anns.getAllAnnotations().size());
		
		// no rule should fire, no blackterms
		ITextUnit tu2 = new TextUnit("id2", "This is a sentence with source 1.");
		tu2.setTargetContent(LocaleId.FRENCH, new TextFragment("This is a sentence with target 1."));
		langTool.run(tu2);
		anns = tu2.getTarget(LocaleId.FRENCH).getAnnotation(ITSLQIAnnotations.class);
		assertNull(anns);
		
		// another blackterm
		tu = new TextUnit("id", "This is a sentence with another blackterm.");
		tu.setTargetContent(LocaleId.FRENCH, new TextFragment("This is a sentence with the blackterm4."));
		langTool.run(tu);
		anns = tu.getTarget(LocaleId.FRENCH).getAnnotation(ITSLQIAnnotations.class);
		assertNotNull(anns);
		assertEquals(1, anns.getAllAnnotations().size());
	}
	
	@Test
	public void termCheckBlacklistJapanese() throws IOException {
		Parameters p = new Parameters();
		p.setTermCheck(false);
		p.setBlacklistCheck(true);
		p.setCheckGrammar(false);
		p.setCheckSpelling(false);
		p.setBlacklistCheckPath(FileLocation.fromClass(this.getClass()).in("/black_tsv_simple_JA.txt").asPath().toAbsolutePath().toString());
		LanguageTool langTool = new LanguageTool(p, LocaleId.ENGLISH, LocaleId.JAPANESE);
		
		// target contains Japanese blackterm "だけ"
		ITextUnit tu = new TextUnit("id", "Srcwrd Srcwrd Srcwrd Srcwrd. ");
		tu.setTargetContent(LocaleId.JAPANESE, new TextFragment("私はあなただけを愛しています。"));
		langTool.run(tu);
		ITSLQIAnnotations anns = tu.getTarget(LocaleId.JAPANESE).getAnnotation(ITSLQIAnnotations.class);
		assertNotNull(anns);
		assertEquals(1, anns.getAllAnnotations().size());
	}

	@Test
	public void termCheckJapanese() throws IOException {
		Parameters p = new Parameters();
		p.setTermCheck(true);
		p.setBlacklistCheck(false);
		p.setCheckGrammar(false);
		p.setCheckSpelling(false);
		p.setTermCheckPath(FileLocation.fromClass(this.getClass()).in("/test_JA.csv").asPath().toAbsolutePath().toString());
		LanguageTool langTool = new LanguageTool(p, LocaleId.ENGLISH, LocaleId.JAPANESE);

		// target contains Japanese term "だけ"
		ITextUnit tu = new TextUnit("id", "This is only a test. ");
		tu.setTargetContent(LocaleId.JAPANESE, new TextFragment("私はあなただけを愛しています。"));
		langTool.run(tu);
		ITSLQIAnnotations anns = tu.getTarget(LocaleId.JAPANESE).getAnnotation(ITSLQIAnnotations.class);
		assertNull(anns);

		// target does not contain Japanese term "だけ"
		ITextUnit tu2 = new TextUnit("id", "This is only a test. ");
		tu2.setTargetContent(LocaleId.JAPANESE, new TextFragment("私はあなたを愛しています。"));
		langTool.run(tu2);
		anns = tu2.getTarget(LocaleId.JAPANESE).getAnnotation(ITSLQIAnnotations.class);
		assertNotNull(anns);
		assertEquals(1, anns.getAllAnnotations().size());

		// target does not contain English term "only"
		ITextUnit tu3 = new TextUnit("id", "This is xxxx a test. ");
		tu3.setTargetContent(LocaleId.JAPANESE, new TextFragment("私はあなただけを愛しています。"));
		langTool.run(tu3);
		anns = tu3.getTarget(LocaleId.JAPANESE).getAnnotation(ITSLQIAnnotations.class);
		assertNull(anns);

		// target contains Japanese term "ビジネス"
		ITextUnit tu4 = new TextUnit("id", "And now, as people return to the office and hybrid work becomes more " +
				"permanent, video communications and virtual events remain critical for business productivity. ");
		tu4.setTargetContent(LocaleId.JAPANESE, new TextFragment("そして今、人々が社内オフィスに戻りつつもハイブリッド " +
				"ワークがより永続的な働き方に変わっていく中で、ビデオ コミュニケーションとバーチャル イベントは引き続きビジネスの生産性の要です。"));
		langTool.run(tu4);
		anns = tu4.getTarget(LocaleId.JAPANESE).getAnnotation(ITSLQIAnnotations.class);
		assertNull(anns);

		// target does not contain Japanese term "ビジネス"
		ITextUnit tu5 = new TextUnit("id", "And now, as people return to the office and hybrid work becomes more permanent, " +
				"video communications and virtual events remain critical for business productivity. ");
		tu5.setTargetContent(LocaleId.JAPANESE, new TextFragment("そして今、人々が社内オフィスに戻りつつもハイブリッド " +
				"ワークがより永続的な働き方に変わっていく中で、ビデオ コミュニケーションとバーチャル イベントは引き続きの生産性の要です。"));
		langTool.run(tu5);
		anns = tu5.getTarget(LocaleId.JAPANESE).getAnnotation(ITSLQIAnnotations.class);
		assertNotNull(anns);
		assertEquals(1, anns.getAllAnnotations().size());
	}
	
	@Test
	public void termCheckOnSpellCheckOff() throws IOException {
		Parameters p = new Parameters();
		p.setTermCheck(true);
		p.setBlacklistCheck(false);
		p.setCheckGrammar(false);
		p.setCheckSpelling(false);
		p.setTermCheckPath(null);
	    p.setBlacklistCheckPath(null);
		LanguageTool langTool = new LanguageTool(p, LocaleId.ENGLISH, LocaleId.fromString("es"));		
		// term check rule will fire as the target term is not exactly as it should be
		ITextUnit tu = new TextUnit("id", "abcdef");
		tu.setTargetContent(LocaleId.fromString("es"), new TextFragment("abcdefg"));
		langTool.setTerms(new LinkedList<ConceptEntry>(), new LinkedList<BlackTerm>());
		langTool.run(tu);
		ITSLQIAnnotations anns = tu.getTarget(LocaleId.fromString("es")).getAnnotation(ITSLQIAnnotations.class);
		assertNull(anns);
	}
	
	@Test
	public void termCheckBasicTbx() throws IOException {
		Parameters p = new Parameters();
		p.setTermCheck(true);
		p.setCheckGrammar(false);
		p.setCheckSpelling(false);
		p.setTermCheckPath(FileLocation.fromClass(this.getClass()).in("/en_es.tbx").asPath().toAbsolutePath().toString());
		LanguageTool langTool = new LanguageTool(p, LocaleId.US_ENGLISH, LocaleId.fromString("es-ES"));
		
		// term check rule will fire as the target term is not exactly as it should be
		ITextUnit tu = new TextUnit("id", "I'll be back before you know it!");
		tu.setTargetContent(LocaleId.fromString("es-ES"), new TextFragment("Voy a estar de vuelta antes de que usted lo sepa!"));
		langTool.run(tu);
		ITSLQIAnnotations anns = tu.getTarget(LocaleId.fromString("es-ES")).getAnnotation(ITSLQIAnnotations.class);
		assertNotNull(anns);
		assertEquals(6, anns.getAllAnnotations().size());
	}
}
