/*===========================================================================
  Copyright (C) 2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.steps.languagetool;

import java.io.IOException;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.UsingParameters;
import net.sf.okapi.common.exceptions.OkapiIOException;
import net.sf.okapi.common.pipeline.BasePipelineStep;
import net.sf.okapi.common.pipeline.annotations.StepParameterMapping;
import net.sf.okapi.common.pipeline.annotations.StepParameterType;
import net.sf.okapi.common.resource.ITextUnit;

@UsingParameters(Parameters.class)
public class LanguageToolStep extends BasePipelineStep {

	private Parameters params;
	private LanguageTool langTool;
	private LocaleId sourceLocale;
	private LocaleId targetLocale;

	public LanguageToolStep() {
		this.params = new Parameters();
	}

	@Override
	public String getName () {
		return "LanguageTool";
	}

	@Override
	public String getDescription () {
		return "Invokes the LanguageTool checker to verify style, grammar, spelling or bilingual terminology errors. "
			+ "Expects: filter events. Sends back: filter events.";
	}

	@Override
	public IParameters getParameters () {
		return params;
	}

	@Override
	public void setParameters (IParameters params) {
		this.params = (Parameters) params;
	}

	@StepParameterMapping(parameterType = StepParameterType.TARGET_LOCALE)
	public void setTargetLocale (LocaleId targetLocale) {
		this.targetLocale = targetLocale;
	}

	@StepParameterMapping(parameterType = StepParameterType.SOURCE_LOCALE)
	public void setSourceLocale (LocaleId sourceLocale) {
		this.sourceLocale = sourceLocale;
	}

	@Override
	protected Event handleTextUnit (Event event) {
		// Initialize the LT object if needed
		if (langTool == null || !langTool.isInitialized()) {
			try {
			langTool = new LanguageTool(params, sourceLocale, targetLocale);
			} catch(IOException e) {
				throw new OkapiIOException("Error creating LanguageTool rules.", e);
			}
		}

		ITextUnit tu = event.getTextUnit();
		langTool.run(tu);
		
		return event;
	}
	
	@Override
	public void destroy() {
		if ( langTool != null ) {
			langTool.shutDown();
		}
	}
	
	/**
	 * Set a custom language tool object
	 * @param lt
	 */
	public void setLanguageTool(LanguageTool lt) {
		this.langTool = lt;
	}
}
