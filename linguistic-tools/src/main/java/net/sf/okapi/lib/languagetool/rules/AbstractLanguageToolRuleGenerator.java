package net.sf.okapi.lib.languagetool.rules;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.languagetool.AnalyzedToken;
import org.languagetool.AnalyzedTokenReadings;
import org.languagetool.Language;
import org.languagetool.rules.patterns.PatternToken;

import net.sf.okapi.lib.languagetool.LanguageToolUtil;

public abstract class AbstractLanguageToolRuleGenerator {
	protected List<PatternToken> getPatternTokens(String term, Language lang) throws IOException {
		List<PatternToken> patterns = new ArrayList<>();
		List<AnalyzedTokenReadings> tokens = LanguageToolUtil.tokenize(term, lang);
		for (AnalyzedTokenReadings readings : tokens) {
			// remove non-word only tokens
			if (readings.isWhitespace() || 
					readings.isLinebreak() || 
					readings.isSentenceEnd() || 
					readings.isSentenceStart()) {
				continue;
			}
			
			// single lemma (if available) for the token, we only need one rule
			if (readings.hasSameLemmas()) {
				patterns.add(createPatternToken(readings.getReadings().get(0)));
			} else {			
				// ambiguous readings - just match on the token - not the lemmas
				PatternToken pt = new PatternToken(readings.getToken(), false, false, false);
				pt.setPhraseName("term");
				patterns.add(pt);
			}						
		}
		return patterns;
	}
	
	protected PatternToken createPatternToken(AnalyzedToken at) {
		// check we we have a lemma or the raw token
		boolean isLemma = true;
		String lemmaOrToken = at.getLemma();
		if (at.getLemma() == null) {
			isLemma = false;
			lemmaOrToken = at.getToken();
		} 
		
		PatternToken pt = new PatternToken(lemmaOrToken, false, false, isLemma);
		pt.setPhraseName("term");
		return pt;
	}
}
