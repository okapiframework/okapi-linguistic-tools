/*===========================================================================
  Copyright (C) 2016 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.lib.languagetool;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;

import org.languagetool.Language;
import org.languagetool.UserConfig;
import org.languagetool.language.Contributor;
import org.languagetool.rules.CommaWhitespaceRule;
import org.languagetool.rules.DoublePunctuationRule;
import org.languagetool.rules.MultipleWhitespaceRule;
import org.languagetool.rules.Rule;
import org.languagetool.rules.SentenceWhitespaceRule;
import org.languagetool.rules.UppercaseSentenceStartRule;
import org.languagetool.rules.patterns.AbstractPatternRule;
import org.languagetool.tokenizers.WordTokenizer;

/**
 * Default {@link Language} used when no other LanguageTool language is
 * available. Uses default {@link WordTokenizer} and a small list of style
 * rules.
 * 
 * @author jimh
 *
 */
public class DefaultLanguage extends Language {

	@Override
	public String getShortCode() {
		return "default";
	}

	@Override
	public String getName() {
		return "Default";
	}

	@Override
	public String[] getCountries() {
		return new String[] {};
	}

	@Override
	public Contributor[] getMaintainers() {
		return new Contributor[] {};
	}

	@Override
	protected synchronized List<AbstractPatternRule> getPatternRules() throws IOException {
		return Collections.emptyList();
	}

	@Override
	public List<Rule> getRelevantRules(ResourceBundle messages, UserConfig userConfig, Language motherTongue,
			List<Language> altLanguages) throws IOException {
		return Arrays.asList(new CommaWhitespaceRule(messages, null, null), new DoublePunctuationRule(messages),
				new UppercaseSentenceStartRule(messages, this, null, null), new MultipleWhitespaceRule(messages, this),
				new SentenceWhitespaceRule(messages));
	}
}
