/*===========================================================================
  Copyright (C) 2016 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.lib.languagetool.rules;

import java.io.IOException;

import org.languagetool.AnalyzedSentence;
import org.languagetool.rules.RuleMatch;
import org.languagetool.rules.bitext.BitextRule;
import org.languagetool.rules.patterns.PatternRule;

/**
 * A Term Check pattern rule class. A TermCheckRule describes a language error
 * where the source term is found, but not the target term.
 * 
 * @author jimh
 */
public class TermCheckRule extends BitextRule {

	private final PatternRule srcRule;
	private final PatternRule trgRule;

	public TermCheckRule(final PatternRule src, final PatternRule trg) {
		srcRule = src;
		trgRule = trg;
	}

	public PatternRule getSrcRule() {
		return srcRule;
	}

	public PatternRule getTrgRule() {
		return trgRule;
	}

	@Override
	public String getDescription() {
		return srcRule.getDescription();
	}

	@Override
	public String getMessage() {
		return trgRule.getMessage();
	}

	@Override
	public String getId() {
		return srcRule.getId();
	}

	/**
	 * This method always returns an empty array. Use
	 * {@link #match(org.languagetool.AnalyzedSentence, org.languagetool.AnalyzedSentence)}
	 * instead.
	 */
	@Override
	public RuleMatch[] match(AnalyzedSentence sentence) throws IOException {
		return new RuleMatch[0];
	}

	@Override
	public RuleMatch[] match(AnalyzedSentence sourceSentence, AnalyzedSentence targetSentence) throws IOException {
		RuleMatch[] matches = srcRule.match(sourceSentence);
		if (matches.length > 0) {
			// if we can't find the specified target term we have an error
			if (trgRule.match(targetSentence).length <= 0) {
				return matches;
			}
		}

		// otherwise we didn't find the source or the source and target terms
		// match the term database - no term check issues
		return new RuleMatch[0];
	}

	@Override
	public String toString() {
		return getSrcRule().toPatternString();
	}
}
