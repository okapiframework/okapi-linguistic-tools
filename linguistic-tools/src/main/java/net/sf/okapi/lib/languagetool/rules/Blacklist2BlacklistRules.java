/*===========================================================================
  Copyright (C) 2016 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.lib.languagetool.rules;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.languagetool.Language;
import org.languagetool.rules.ITSIssueType;
import org.languagetool.rules.patterns.PatternRule;
import org.languagetool.rules.patterns.PatternToken;

import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.exceptions.OkapiException;
import net.sf.okapi.lib.languagetool.LanguageToolUtil;
import net.sf.okapi.lib.verification.BlackTerm;
import net.sf.okapi.lib.verification.BlacklistReader;

public class Blacklist2BlacklistRules extends AbstractLanguageToolRuleGenerator {
	private static final String DESCRIPTION = "Target Blackterm Check";
	private static final String MESSAGE = "Target contains a blackterm: \"%s\".%s";
	private static final String SHORT_MESSAGE = "Blackterm Check";

	// LanguageTool Language objects
	private Language trgLang;
	private List<BlackListRule> rules;

	public Blacklist2BlacklistRules(LocaleId trgLocale) {
		this.trgLang = LanguageToolUtil.getCachedLanguage(trgLocale);
		rules = new ArrayList<>();
	}

	public List<BlackListRule> convert(List<BlackTerm> blackTerms) throws IOException {
		if (blackTerms == null) {
			throw new OkapiException("BlackTerm list is null.");
		}
		rules.clear();
		for (BlackTerm bt : blackTerms) {
			rules.add(convertToBitextPatternRule(bt.text, bt.suggestion));
		}
		return rules;
	}

	public List<BlackListRule> convert(BlacklistReader reader) throws IOException {
		if (reader == null) {
			throw new OkapiException("Blacklist Reader is null.");
		}
		rules.clear();
		try {
			while (reader.hasNext()) {
				BlackTerm bt = reader.next();
				rules.add(convertToBitextPatternRule(bt.text, bt.suggestion));
			}
		} finally {
			reader.close();
		}

		return rules;
	}

	private BlackListRule convertToBitextPatternRule(String blackTerm, String suggestion) throws IOException {
		List<PatternToken> trgTokens = getPatternTokens(blackTerm, trgLang);
		PatternRule trgPattern = new PatternRule(blackTerm, trgLang, trgTokens, DESCRIPTION,
				String.format(MESSAGE, blackTerm, 
						Util.isEmpty(suggestion) ? "" : String.format(" A suggested replacement is: <suggestion>%s</suggestion>", suggestion)), 
				SHORT_MESSAGE);
		BlackListRule r = new BlackListRule(trgPattern, SHORT_MESSAGE);
		r.setLocQualityIssueType(ITSIssueType.Terminology);		
		return r;
	}

	public Language getTrgLang() {
		return trgLang;
	}
}
